import os
from uuid import uuid4
from bitbucket_pipes_toolkit import Pipe, CodeInsights

variables = {
	'BITBUCKET_USERNAME': {'type': 'string', 'required': False, 'default': os.getenv('BITBUCKET_REPO_OWNER')},
	'BITBUCKET_REPOSITORY': {'type': 'string', 'required': False, 'default': os.getenv('BITBUCKET_REPO_SLUG')},
}
pipe = Pipe(schema=variables)

REPORT_ID = "prettier"

COMMIT = os.getenv('BITBUCKET_COMMIT')


def get_insights():
	return CodeInsights(
		repo=pipe.get_variable('BITBUCKET_REPOSITORY'),
		username=pipe.get_variable('BITBUCKET_USERNAME')
	)


def upsert_report(failsCount):
	insights = get_insights()
	# try:
	# 	insights.delete_report(COMMIT, REPORT_ID)
	# except Exception as e:
	# 	pipe.log_error(str(e.args))

	details = "All files are properly formatted"
	result = "PASSED"
	if failsCount > 0:
		details = f"{failsCount} files were not formatted"
		result = "FAILED"
	report_data = {
		"type": "report",
		"report_type": "BUG",
		"title": "Prettier report",
		"details": details,
		"result": result,
		"reporter": "created by my pipe",
		"external_id": REPORT_ID,
	}
	report = insights.create_report(COMMIT, report_data=report_data)
	return report


def create_annotation_for(fileName):
	return {
		"annotation_type": "CODE_SMELL",
		"external_id": str(uuid4()),
		"summary": "This file is not formatted correctly",
		"severity": "LOW",
		"result": "FAILED",
		"line": 0,
		"path": fileName
	}


def annotate_files(files):
	insights = get_insights()

	if len(files) == 0:
		pipe.log_info("No errors were found")
		return
	annotations = [create_annotation_for(file) for file in files]
	for annotation in annotations:
		annotation = insights.create_annotation(COMMIT, REPORT_ID, annotation)
		pipe.log_info("Created annotation")
		pipe.log_info(annotation)


def main():
	pipe.log_info("Scanning for wrongly formatted files")

	filesWithIncorrectFormatting = [".gitignore"]
	failsCount = len(filesWithIncorrectFormatting)
	try:
		upsert_report(failsCount)
	except Exception as e:
		pipe.log_error("Error occurred while upserting report")
		pipe.log_error(str(e))
		exit(2)
	try:
		annotate_files(filesWithIncorrectFormatting)
	except Exception as e:
		pipe.log_error("Error occured while creating annotations")
		pipe.log_error(str(e))
		exit(3)
	if failsCount > 0:
		exit(1)


if __name__ == '__main__':
	main()
